// main.js
var app = angular.module('littleSketcher', ['ngResource', 'ngRoute']);

app.config(function($routeProvider){
    $routeProvider
        .when("/",{
            templateUrl: 'tpl/index.html',
            controller: 'rootCtrl'
        })
        .when("/drawings", {
            templateUrl: 'tpl/drawings.html',
            controller: 'drawingListCtrl'
        })
        .when("/drawings/new", {
            templateUrl: 'tpl/drawings_create.html',
            controller: 'drawingCreateCtrl'
        })
        .when("/drawings/:id", {
            template: 'tpl/drawings_create.html',
            controller: 'drawingCreateCtrl'
        });
});

app.factory("DrawingResource", function($resource){
    var DrawingResource = $resource("/api/drawings/:id", {id: "@id"});
    return DrawingResource;
});

app.controller('rootCtrl', function($scope){
  console.log('rootCtrl');
});

app.controller('drawingCreateCtrl', function($scope, $routeParams){
  console.log(JSON.stringify($routeParams));
});

app.controller('drawingListCtrl', function($scope, DrawingResource){
    $scope.drawings = DrawingResource.query();

    $scope.remove = function(drawing){
        var diRemove = drawing.$remove();
            diRemove.then(function(){
                var index = $scope.drawings.indexOf(drawing);
                $scope.drawings.splice(index, 1);
            }, function(){
                alert('oh no!!');
            });
    };

    $scope.addDrawing = function(){
        var newDrawing = new DrawingResource();
            newDrawing.$save();

        $scope.drawings.push(newDrawing);
    };
});

app.controller('rootCtrl', function(){
    console.log('oi');
});


app
  .controller('drawingCreateCtrl', function($scope, $routeParams){
    console.log(JSON.stringify($routeParams));
    $scope.drawing = {
      name: 'new drawing'
    };
  })
  .directive('toggleInput',function($document){
    return {
      scope:{
        toggledValue: "=toggleInput"
      },
      template:[
        "<div>",
        "{{ directiveArgument }}",
        "<div ng-transclude ng-hide='input.visible'></div>",
        " <form>",
        "   <input ng-model='toggledValue' ng-show='input.visible'>",
        " </form>",
        "</div>",
      ].join(""),
      // pul in DOM of whole element, not just contents
      transclude: "element",
      replace: true, // replace whole element, not ust content
      link: function link(scope,el,attrs){
        console.log(el);

        scope.input = {
          visible: false
        };

        var input = el.find('input');
        var bodyClickHandler;

        el.find('[ng-transclude]').on('click', function(){
          scope.$apply(function(){
            scope.input.visible = true;
            setTimeout(on);
          });
        });

        function on(){
          $document.on('click', bodyClickHandler =  function(event){
            if($(event.target).is(input)){
              return;
            }
            off();
          });
        }

        function off(){
          $document.off('click', bodyClickHandler);
          scope.$apply(function(){
            scope.input.visible = false;
          });
        }
      }
    };
  });
